[OpenStack](https://www.openstack.org/) training materials by the [Software Freedom School](http://www.sofree.us/)

Get latest from: https://gitlab.com/sofreeus/AGHI2OpenStack

# Plan

1. Setup the learning environment.

  [00_setup](labs/00_setup)

* Discussion

  - What is Cloud Computing?
    differences to phys/virt/kube?
    * multi-tenancy: highly resistant to noisy and nosy neighbors
    * quota, not VMs: processors, memory, storage, network (addresses, bandwidth)
    * HA & Failover: virt clusters and cloud
    * Services: volume, dns, file buckets, databases, kubernetes, mesos
  - What is OpenStack?
    * Datacenter software for provisioning cloud tenants
    * FLOSS
    * Customer Mobility
  - Who uses OpenStack? telecoms and SFS
  - How does OpenStack compare to AWS, GCE, Azure, and so on?

* Create a Project in existing OpenStack cluster

  [05_openstack_project_setup](labs/05_openstack_project_setup)

* Create resources in the OpenStack GUI

  [10_openstack_gui](labs/10_openstack_gui)

* Create resources from the OpenStack CLI

  [20_openstack_cli](labs/20_openstack_cli)

* Configure a DevStack single-node OpenStack cluster from an image

  [40_devstack](labs/40_devstack/)

See also:

[Component Service Architecture](https://docs.openstack.org/security-guide/_images/marketecture-diagram.png)

[Upstream's Intro to OpenStack](https://docs.openstack.org/security-guide/introduction/introduction-to-openstack.html)
