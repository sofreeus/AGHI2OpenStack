This repository is all original materials by the Software Freedom School.

You are granted the four freedoms (use, study, share, modify). You may create derivative products and use bits from this repo in other products, as long as you give credit for whatever you re-use and grant users of your derivative or importing product the same four freedoms.

    code: GPLv3 or AGPL, at your option
    else: CC BY SA
