# OpenStack Orchestration

Heat, Ansible, Terraform, etc...

## Talk

- Heat stacks are atomic!
- Fuga Cloud *rocks*! [Fuga tuts](https://docs.fuga.cloud/tutorials)

## Demo and Pair

### Get access to Fuga Cloud

- instructor invites students by email address
- Get openrc file:
    * Go to https://my.fuga.cloud/account/access
    * Click "Create more API Credentials"
    * Record the password safely and securely!
    * Download the OpenRC file
- Verify that the OpenRC file works

```bash
. ~/Downloads/openrc.sh
# enter the password you stored, when prompted
openstack server list
openstack stack list
```

FIXME More. Much more.

## Share

What's the big, important difference between a Heat stack and a similar collection of resources created by script?
