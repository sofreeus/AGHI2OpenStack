# Guest is black screen

If your Learn Station's screen goes black and stays black, try rebooting. If that doesn't work, try increasing the video memory. 


# Mac not installs VirtualBox

If a Mac is not installing VirtualBox, you need to click `Allow` in System Preferences --> Security & Privacy --> General.

If you can't click it (because, say, you have some 3rd party mouse driver installed)...

Open Script Editor.

Position Script Editor and your System Preferences window on your screen such that both are visible.

Type CMD+CTRL+SHIFT+4 to get screenshot crosshairs, and use them to find the coordinates of the `Allow` button.

Type ESC to avoid taking a screenshot.

In Script Editor, enter:

`tell application "System Events" to click at {880, 511}`

Replacing `880` and `511` with the coordinates for your `Allow` button.

Click the play button to run the script.
