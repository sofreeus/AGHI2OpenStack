# Setup

## Talk

In order to ensure that things work as expected, we want everyone using basically the same environment. So, authors write and test against the environment in which teachers will demonstrate and participants will do the Pair Labs. In this way, we can minimize time-consuming surprises.

## Setup a LearnStation in VirtualBox

Install or update VirtualBox from www.virtualbox.org

## Get the class VM

Load https://nextcloud.sofree.us/index.php/s/Kg3z3Z2dB5rOpnH in your browser
then download the most recent Ubuntu Disco Learner Station image from
the list of files. 
It'll have a name like "SFS-LearnStation-Ubuntu-Disco.ova".
The download may take a few minutes.

There will also be USB sticks with this image available.  Ask the
instructor or helper for one if you'd rather load the image that way.

Additionally, at your option, download other LearnStations and try the exercises in them.

## Import the Image

In VirtualBox, import the Learner Station with File, Import Appliance.

## Sign into the VM

Sign in as osadmin with the usual password. If you don't know it, ask your instructor, email captains@sofree.us, or ask in [Mattermost](https://mattermost.sofree.us)

## Select an editor
If you don't have an editor preference, consider installing Atom.

```bash
snap install --classic atom
```

## Fetch the materials
Fetch the class materials to your learning machine.

```bash
wget https://www.sofree.us/bits/git-a-class
```

Review the `git-a-class` script:

```bash
VIEW git-a-class
```

Make it executable and run it:
```bash
chmod +x git-a-class
./git-a-class aghi2openstack
mv git-a-class ~/sfs/
```

### Typical problem resolution

If you get an error about not being able to acquire an apt lock, wait 5-10 minutes for the system to settle, or if you're impatient, reboot and try again.

## Install the rest of the class requirements.

```bash
cd ~/sfs/aghi2openstack/labs/00_setup/
less get-reqd # or use the viewer of your choice
chmod +x get-reqd
./get-reqd
```

## Add special routes as needed

For many DevStack exercises, you will need to reach various ports on the HOST_IP and addresses in the FLOATING_RANGE.

You may need to do one or more of:
- Be at SFS HQ (The Comfy Room)
- Tunnel in by SSH or VPN
- Create a special route

`sshuttle` gives us a simple VPN-like experience by automatically configuring TCP-only tunneled routes.

To access the HOST_IP and FLOATING_RANGE of the DevStack we're using in October 2019, we'll connect sshuttle to brownberry.sofree.us. This will enable HTTP, HTTPS, and SSH but not ping, because ping is ICMP, not TCP.

```bash
# run the following command in a dedicated tab or terminal window
sshuttle -r stacker@brownberry.sofree.us -v 192.168.73.{86..89} 172.24.4.0/24
# you need to replace 172.24.4.0/24 if a different FLOATING_RANGE was specified in DevStack local.conf
# you need to replace 192.168.73.86 if a different HOST_IP was specified in DevStack local.conf
```

For the rest of the class, ignore, but do not close, the terminal tab in which you run the sshuttle command.
