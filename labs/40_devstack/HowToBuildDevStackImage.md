# How To Build The DevStack Image

## Build

Create a New VirtualBox machine
- 8GB RAM
- 2 cores
- 32 GB HDD
- Bridged Network Adapter

Install Ubuntu Server 16.04.3 AMD64
- user: osadmin:SoFreeUs-2017
- hostname: DevStack

Update everything

```
sudo apt update &&
sudo apt -y dist-upgrade &&
sudo apt-get autoremove &&
sudo reboot
```

Enable byobu
`byobu-enable`

Enable sshd
`apt install openssh-server`

Follow [DevStack quick-start](https://docs.openstack.org/devstack/latest/)
- Create `local.conf` with all passwords set to SoFreeUs-2017
- run `stack.sh`
- run `clean.sh`
- poweroff

Create an archive. From VirtualBox Manager, File, Export Appliance.

Test the archive by doing the [DevStack Exercises](ExercisesForDevStack.md).

Upload the archive and/or copy it to USB sticks.
