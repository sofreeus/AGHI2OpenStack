Exercises for DevStack
===

- Download the most recent DevStack OVA from https://nextcloud.sofree.us/index.php/s/Kg3z3Z2dB5rOpnH or from a provided USB stick.
- Or, make one using the [How To](HowToBuildDevStackImage.md).

**Important:** Make sure to check "Re-initialize the MAC address..." when you import.

You may need to update the attachment for the VirtualBox bridged adapter to your connected physical adapter.

Determine the ip address of the DevStack machine by logging into the console and running `ip a` then <control>+d to logout.

Do everything else you do from the DevStack machine through ssh.

`ssh osadmin@$DevStack_IP`

Example: `ssh osadmin@192.168.0.65`

```bash
# Impersonate the stack user.
sudo su - stack

# Make `~/devstack/` the working directory.
cd ~/devstack/

# Update the DevStack code
git pull

# Switch to the desired branch.
# The last one that has worked from me is queens, but you may want pike or rocky.
git checkout stable/queens
```

Ensure good DevStack settings.

`EDIT local.conf`

Are the passwords what you want?

Confirm that FLOATING_RANGE is not in use anywhere else in the routing domain.
The default FLOATING_RANGE is 172.24.4.0/24.
You can change it by adding a line like `FLOATING_RANGE=$NETWORK_IN_CIDR`.
Example: `FLOATING_RANGE=192.168.73.0/24`


Configure up DevStack.

`./stack.sh`

Sign in at web console.

`http://$DevStack_IP`

Create two instances:
- Select the demo project.
- In Details, enter an "Instance Name" that makes sense to you. Maybe "foo" and "bar", "alpha" and "beta", or "Alice" and "Bob".
- In Source, change "Delete Volume on Instance Delete" from "No" to "Yes" and choose the 'cirros' image.
- In Flavor, choose a flavor that has small, positive memory and storage.

Take note of the IP addresses.

Connect to the console of an instance.
- Click Project/Network/Network Topology
- Hover over an instance.
- Click "Open Console"
- Log in. The credentials are right there.

## Networking: Floating IP's, Security Groups, and routing

Confirm that this instance can ping the other instance (proves that virtual ethernet is good), 8.8.8.8 (proves that my gateways are good to the Internet), and duckduckgo.com (proves that my DNS is good).

```
ping -c 3 $PEER_IP
ping -c 3 8.8.8.8
ping -c 3 duckduckgo.com
curl ifconfig.me # what is my public Internet IP
```

Assign a Floating IP to the `demo` project.
- Click Admin/Network/Floating IPs
- Click "Allocate IP to Project"

Associate the floating IP with an instance.
- Click Project/Compute/Instances or Project/Network/Floating IPs

From the DevStack host, confirm that you can ping the floating range router interface (generally .1), but *not* the floating ip you just associated with the instance.

Example:

```
ping -C3 192.168.73.1   # success is success
ping -C3 192.168.73.14  # failure is success
```

Add a security group that allows "all ICMP" from any host "0.0.0.0/0" and add that security group to the same instance with which you associated the floating ip. Verify that you can now ping the floating IP from the DevStack host.

Note that other hosts (probably) don't have a route to the floating range.

To add a temporary route to a Linux machine, try something like:

`ip route add $FLOATING_RANGE via $DevStack_IP`

Example: `sudo ip route add 192.168.73.0/24 via 192.168.0.65`

Ping the router of floating range and the pingable instance to confirm.

Verify that although you can ping the host with the floating IP, you can't connect to it with SSH.

```
ssh cirros@$FLOATING_IP # should hang
```

Add a security group that allows "SSH", add that security group to the host associated with the floating IP.

Verify that ssh `cirros@$FLOATING_IP` works now.

Example: `ssh cirros@192.168.73.14`

Add the same route to a gateway router, which will make the floating range available to machines that use that gateway.

---

## CLI

Get `openstack`. (varies)

Download the RC file from the GUI. (upper-right corner)

Source the RC file.

```
$ . ~/Downloads/demo-openrc.sh
Please enter your OpenStack Password for project demo as user admin:
```

List some of the things.

```
$ openstack server list
+--------------------------------------+-------+--------+----------------------------------------+------------+
| ID                                   | Name  | Status | Networks                               | Image Name |
+--------------------------------------+-------+--------+----------------------------------------+------------+
| c8550cc8-06a2-47b5-8c53-e4e6f4284879 | alpha | ACTIVE | private=10.0.0.10,                     |            |
|                                      |       |        | fd22:7fad:20e8:0:f816:3eff:fe39:db2,   |            |
|                                      |       |        | 172.24.4.11                            |            |
+--------------------------------------+-------+--------+----------------------------------------+------------+

$ openstack volume list
+--------------------------------------+--------------+--------+------+--------------------------------+
| ID                                   | Display Name | Status | Size | Attached to                    |
+--------------------------------------+--------------+--------+------+--------------------------------+
| 5853ef6c-ae9f-47da-b4f4-eced20ba2f2c |              | in-use |    1 | Attached to alpha on /dev/vda  |
+--------------------------------------+--------------+--------+------+--------------------------------+

$ openstack image list
+--------------------------------------+--------------------------+--------+
| ID                                   | Name                     | Status |
+--------------------------------------+--------------------------+--------+
| 0e93fcf4-6b9f-43f4-9d97-c61e9a81848a | cirros-0.3.5-x86_64-disk | active |
+--------------------------------------+--------------------------+--------+
```

Create a third instance.
