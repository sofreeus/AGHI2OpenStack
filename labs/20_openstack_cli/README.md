# OpenStack CLI

## Talk

- CLI is scriptable
- scripts produce consistent results quickly
- scripts can be made portable
- scripts are more clear and lasting than click-by-click instructions
- OTOH: it does take longer to write a script

## Demo and Pair

Get the openrc file for a project user, not the admin user.

- Sign in to [Horizon](https://openstack.sofree.us) as a **project** user, not admin.
- Browse https://openstack.sofree.us/dashboard/project/api_access/openrc/
- View or edit it.

### Explore

```sh
# Run your openrc file
. ~/Downloads/${MACGUFFIN}-openrc.sh

openstack user list
openstack group list
openstack role list

openstack project list
openstack router list
openstack network list
openstack subnet list
openstack port list

openstack server list
openstack flavor list
openstack image list
```

### Create a keypair

```sh
keyname=$OS_PROJECT_NAME-$( date --iso )
echo $keyname

# by uploading a public key you created
# create a keypair
ssh-keygen -f ~/.ssh/$keyname
# upload the public-key
openstack keypair create --public-key ~/.ssh/$keyname.pub $keyname

# or, by downloading a private key OpenStack created
# openstack keypair create $keyname > ~/.ssh/${keyname}.pem
# chmod go-rwx ~/.ssh/${keyname}.pem

openstack keypair list
```

### Create some security groups

```bash
openstack security group list

# add a group for jump-hosts
# if you already have jump-hosts-sg, skip this step
openstack security group create jump-hosts-sg
openstack security group rule create jump-hosts-sg --dst-port 22

# add a group for web-app-hosts
openstack security group create app-hosts-sg
openstack security group rule create app-hosts-sg --dst-port 80
openstack security group rule create app-hosts-sg --dst-port 443

# add a group for database-hosts
openstack security group create db-hosts-sg
openstack security group rule create db-hosts-sg --dst-port 3306 --remote-group app-hosts-sg
openstack security group rule create db-hosts-sg --dst-port 5432 --remote-group app-hosts-sg

# allow jump-hosts to ssh to app-hosts and db-hosts
openstack security group rule create db-hosts-sg --dst-port 22 --remote-group jump-hosts-sg
openstack security group rule create app-hosts-sg --dst-port 22 --remote-group jump-hosts-sg
```

### Create some machines

```sh
image="bionic-minimal"
servers="app db jump"
for server in $servers
do
    openstack server create --image ${image} --flavor ds1G \
    --key-name ${keyname} \
    --network ${OS_PROJECT_NAME}-net \
    --security-group ${server}-hosts-sg \
    ${server}
done
```

### Assign Floating IPs to jump and app

FIXME

### Test it all out!

ssh to jump with forwardagent

```bash
ssh -i ~/.ssh/$keyname -o ForwardAgent=true ubuntu@172.24.4.191
```

ssh from jump to app and start a webserver

```bash
ssh 10.0.0.200
echo 'Well hello Stacker!' > index.html
sudo python3 -m http.server 80
```

browse app by ip address

browse partner's app by ip address

### Bonus Points?

Write this all into a setup script and write a matching tear-down script.

Parameterize the bits that are most likely to change between OpenStack hosts, like image-name, so that the script can be adapted to new OpenStack hosts quickly.

## Share

What did you think of the access rules of these Security Groups? Too tight, or not tight enough?

For those familiar with another cloud CLI, how does OpenStack CLI compare?
