# Getting to know the OpenStack GUI

## Talk

GUI is easy and less easy to screw up!

## Demo and Pair

### Create a new server

Sign in to [Horizon](https://openstack.sofree.us) as a **project** user, not admin.

Create an instance.

Project > Compute > Instances > Launch instance
  * Details:
    + Instance name = 'alpha'
  * Source:
    + Delete volume... = yes
    + Choose the "Cirros..." image
  * Flavour:
    + Choose "cirros256"

### Allocate a Floating IP

Project > Network > Floating IPs

Click the "Allocate IP To Project" button, then click "Allocate IP".

Click "Associate" on the addresses and associate it with 'alpha'.

Project > Compute > Instances

Note that one of the instances has an Floating IP.

### Clean up the default Security Group

Unjustified wide-open loop-back rules are evil. Unjustified loop-back rules that you can deploy on your servers without noticing are insidiously evil.

Project > Network > Security Groups

Delete both wide-open ingress rules

Some people will say that these rules are fine, that un-governed access between hosts in the same security group is usually fine. Those people are wrong.

### Create a Security Group for your jump-hosts

Project > Network > Security Groups

Create a Security Group named "jump-hosts-sg".

Click "Manage Rules" on the "jump-hosts-sg" security group and add a rule to allow SSH, either public, or just your and your partner's apparent IP addresses.

Project > Compute > Instances

Add the "jump-hosts-sg" Security Group to 'alpha'.

Verify that you can ssh to the Floating IP as user 'cirros' with a password of 'gocubsgo'

Verify that you can ssh to your partner's 'alpha' and vice-versa.

Finally, Shut Off or Delete 'alpha'

## Share

Do you agree or disagree with the stance on wide-open loop-back rules?

What is your stance on wide-open ssh vs access-list only? Sometimes? Never? Change the port?
