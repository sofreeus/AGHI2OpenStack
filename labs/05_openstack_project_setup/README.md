# OpenStack

## Talk

OpenStack is a free-libre datacenter software for provisioning private and public cloud infrastructure. It's sort of like AWS, GCP, or Azure, but it's open-source, like Linux.

## Demo and Pair Exercise

### Create a new project

Get the admin password to the OpenStack cluster from an instructor, or ask the instructor to perform the following steps for you.

## Download admin's RC file.

1. Sign in to Horizon, the [OpenStack GUI](https://openstack.sofree.us), as 'admin'.
*  Confirm that the current project is 'admin' in the menu to the right of the OpenStack logo in the upper-left.
*  Download admin's RC file. Click the user control in the upper-right corner, and choose "OpenStack RC File".

```bash
# Source admin's RC file.
less ~/Downloads/admin-openrc.sh # or your preferred viewer
. ~/Downloads/admin-openrc.sh
openstack project list
openstack user list
# this serves the dual purpose of verifying that you set the password correctly
# and giving you a list of project and user names you must not use
```

## Create a new project in the CLI

```bash
# Customize your `project.settings.private` file from one of the examples.
VIEW project.settings.*
cp project.settings.something project.settings.private
EDIT project.settings.private
# Create the new project
VIEW admin.create-project
./admin.create-project project.settings.private
```

## Verify project

Verify project, users, network, and router in the GUI and/or CLI

GUI:

* Projects - https://openstack.sofree.us/dashboard/identity/
* Users - https://openstack.sofree.us/dashboard/identity/users/
* Networks - https://openstack.sofree.us/dashboard/admin/networks/
* Routers - https://openstack.sofree.us/dashboard/admin/routers/

CLI:

```bash
# Projects
openstack project list
# Users
openstack user list
# Networks
openstack network list
# Routers
openstack router list
```

## Share

From memory, what did we just do? i.e. What resources did we create?
